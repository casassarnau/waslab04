package twitter;

import java.util.Date;

import twitter4j.ResponseList;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;


public class SimpleClient {

	public static void main(String[] args) throws Exception {
		
		final Twitter twitter = new TwitterFactory().getInstance();
		/*
		ResponseList<Status> status = twitter.getUserTimeline("fib_was");
		Status lastTweet = status.get(0);
		System.out.println(lastTweet.getText());
		twitter.retweetStatus(lastTweet.getId());
		*/
		final TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
		
		twitterStream.addListener(new StatusListener() {
			@Override
			public void onStatus(Status status) {
				User user = status.getUser();
				String text = user.getName() + " (@" + user.getScreenName() + "): ";
				text += status.getText() + "\n";
				System.out.println(text);
			}

			@Override
			public void onException(Exception arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onDeletionNotice(StatusDeletionNotice arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onScrubGeo(long arg0, long arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStallWarning(StallWarning arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTrackLimitationNotice(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		twitterStream.filter("#covid19");
		
	}
}
